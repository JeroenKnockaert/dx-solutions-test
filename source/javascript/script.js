//NAMELESS FUNCTION - AUTO EXECUTED WHEN DOCUMENT IS LOADED
(function(){
	
	$(".sidebar-nav li a").hover(function(){
		$(this).children().attr("src", function(index, attr){
 
			var id =  this.id;
			var path = 'assets/images/';
			var extention = '.png';
			
			var normalPath = path + id + extention;
			var hoverPath = path + id + '-hover'+ extention;
			
			return attr.replace(attr,hoverPath);

		}); 
	}, function(){
		$(this).children().attr("src", function(index, attr){
			//console.log($(this).parent().parent().hasClass("current"));
			if($(this).parent().parent().hasClass("current")){
			//do nothing
			} else {
				return attr.replace("-hover.png", ".png");
			};
		});

	});
	
    /*
	    Modals
	*/
	$('.launch-modal').on('click', function(e){
		e.preventDefault();
		$( '#' + $(this).data('modal-id') ).modal();
	});




})();