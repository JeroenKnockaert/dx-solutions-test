/**
 * Custom Gulp Tasks.
 * 
 * @author    Jeroen Knockaert
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
	
	
	// grab our gulp packages
	var gulp = require('gulp');
	var gutil = require('gulp-util');
	var concat = require('gulp-concat');
	var sass = require('gulp-sass');
	var minifyCss = require('gulp-minify-css');
	var rename = require('gulp-rename');
	var sh = require('shelljs');



	var paths = {
		sass: ['./source/**/**/*.scss'],
		templates :['./source/**/*.html'],
		scripts :['./source/javascript/**/*.js']
	};
	

	// create a default task and just log a message
	gulp.task('default', function() {
	  return gutil.log('Gulp is running!')
	});
	
	gulp.task('copyHtml', function() {
	  // copy any html files in source/ to public/
	  gulp.src('source/*.html').pipe(gulp.dest('public'));	
	});
	
	
		gulp.watch(paths.sass, ['sass']);
		gulp.watch(paths.templates, ['templates']);
		gulp.watch(paths.scripts, ['scripts']);
		//gulp.watch('source/**/*.html', ['copyHtml']);
	
	// Concatenate all scripts to `./www/js/app.js`.
	gulp.task('scripts', scripts);
	function scripts() {
		return gulp
			.src('./source/**/*.js')
			.pipe(concat('app.js'))
			.pipe(gulp.dest('./public/assets/javascript'));
	}
	
	// Copy all html files to the `./www/templates` folder.
	gulp.task('templates', templates);
	function templates() {
		sh.rm('-rf', './www/templates'); // Delete folder and all files within.
		return gulp
			.src('./source/**/*.html')
			// .pipe(rename({dirname: ''})) // Remove foldernames from path.
			.pipe(gulp.dest('./public'));
	}
	
	gulp.task('default', ['sass']);

	gulp.task('sass', function(done) {
	gulp.src('./source/scss/styles.scss')
		.pipe(sass({
		errLogToConsole: true
		}))
		.pipe(gulp.dest('./public/assets/stylesheets/'))
		.pipe(minifyCss({
		keepSpecialComments: 0
		}))
		.pipe(rename({ extname: '.min.css' }))
		.pipe(gulp.dest('./public/assets/stylesheets/'))
		.on('end', done);
	});
	